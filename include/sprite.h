#pragma once
#include <iostream>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <memory>
#include "window.h"
#include "position.h"
#include "vector2d.h"
class Sprite {

private:

public:
  Sprite(const std::string &image_path);
  ~Sprite();
  void Draw(const Vector2D &position, int width, int height) const;
  void PlayAnimation(int beginframe, int endFrame, int row, int speed);
protected:
  int r=0,b=9,g=255,a=255;
  SDL_Texture *sprite_texture{ nullptr};
  int m_width;
  int m_height;
  SDL_Rect sourceRect;
  SDL_Rect destinationRect;
  int currentFrame;
  int animationDelay;




};
