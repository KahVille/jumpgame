#pragma once
#include <iostream>
#include <SDL2/SDL.h>

class Mouse
{
public:
    Mouse() {}
    bool click();
    int positionX();
    int positionY();
    void HandleEvents(SDL_Event &event);

private:
    //mouse
    int mouseX =0;
    int mouseY=0;
    bool mouseClick=false;

};
