#pragma once
#include <SDL2/SDL_mixer.h>
#include <string>
class Audio
{
public:
    Audio(const std::string &filename);
    ~Audio();

    void PlaySound();


private:
    Mix_Chunk *sound{nullptr};


};
