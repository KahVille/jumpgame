#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include <string>

class Window
{
public:

    Window(const std::string &title, int width, int height);
    virtual ~Window();

    bool isClosed() const;

    void pollEvents(SDL_Event &event);
    void clear() const;
void setResolution(int width, int height);

     static SDL_Renderer *renderer;
private:

    bool init();

     std::string title;
     int width = 0;
     int height = 0;
     bool closed = false;

     SDL_Window* window = nullptr;
protected:

};
