#pragma once

class Position {
private:

public:
Position(int x,int y);

int xCoord() const;
int yCoord() const;
void setxCoord(double x);
void setyCoord(double y);
void setyVelocity(double yvel);
void setxVelocity(double xvel);
int yVelocity() const;
int xVelocity() const;
void update();

private:
double m_xvelocity =0;
double m_yvelocity=0;
double m_x=0,m_y=0;

};
