#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <algorithm>
#include  <utility>
#include "window.h"
#include "player.h"
#include "sprite.h"
#include "kahviui.h"
#include "vector2d.h"
#include "audio.h"

class Game
{
public:
    Game();
    void pollEvent(SDL_Event &event);
    void update();
    void render();
    bool isClosed() {return m_closed;}

    void run();
private:
    //The camera area

    std::unique_ptr<Window> gameWindow {nullptr};
  //std::unique_ptr<kahviUI::Button> button = std::make_unique<kahviUI::Button>(10,10,220,60,"Play");
    bool m_closed{false};
  std::unique_ptr<Player> player {nullptr};
  std::unique_ptr<GameObject> ground {nullptr};
  //Audio sound{"../res/Audio/Sfx/jump.ogg"};




};
