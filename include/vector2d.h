#pragma once
class Vector2D
{
public:
    Vector2D();
    Vector2D(double x,double y);
    Vector2D operator+(const Vector2D& vec2) const;
    Vector2D &operator+=(const Vector2D& vec2);
    Vector2D operator*(double value) const;
    Vector2D &operator*=(double value);
    double x=0,y=0;


};
