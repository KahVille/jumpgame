#pragma once
#include <iostream>
#include <memory>
#include <SDL2/SDL.h>
#include <sprite.h>
#include <position.h>
#include "kahviui.h"
namespace kahviUI {
class Text;
class Button {
public:
    Button(int x, int y,int width, int height, const std::string &buttontext);
    void onClicked();
    void display();
    void setVisible(bool visibility);
    bool isVisible();
    void setPosition(int x, int y);
    int positionX();
    int positionY();
    int width() {return m_width;}
    int height() {return m_height;}

private:
    bool m_state = true;
    Uint8 r{128}, g{0}, b{128}, a{255};
    SDL_Color color = {r,g,b,a};
    std::unique_ptr<Position> position = std::make_unique<Position>(0,0);
    int m_width{0};
    int m_height{0};
    std::unique_ptr<Sprite> sprite = std::make_unique<Sprite>("../res/Graphics/GUI/PlayButton/buttondebug.png");
    std::unique_ptr<kahviUI::Text> text = std::make_unique<kahviUI::Text>(Window::renderer,"../res/Font/OpenSans-Regular.ttf", 28,"",color);

};

}
