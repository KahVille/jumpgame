#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
namespace kahviUI {
class Text
{
public:
    Text(SDL_Renderer *renderer,const std::string &font_path, int font_size, const std::string &message_text, const SDL_Color &color);
    void display(int x, int y, SDL_Renderer *renderer) const;
    static SDL_Texture *loadFont(SDL_Renderer *renderer,const std::string &font_path, int font_size, const std::string &message_text, const SDL_Color &color);
    void setText(const std::string &text, SDL_Renderer *renderer);
     void freeTexture();
    virtual ~Text();
private:
    SDL_Texture* m_text_texture = nullptr;
    mutable SDL_Rect m_text_rect;
    std::string m_text;
    std::string m_font_path;
    int m_font_size;
    SDL_Color m_font_color;



};
}
