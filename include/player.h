#pragma once
#include <memory>
#include <iostream>
#include <SDL2/SDL.h>
#include "sprite.h"
#include "camera.h"
#include "gameobject.h"
#include "vector2d.h"
#include "audio.h"
class Player : public GameObject
{
public:
    Player(Vector2D position);
    void pollEvent(SDL_Event &event);
    void draw();
    //Shows the player on the screen relative to the camera
    void render();
    void update();
    void OnGround(bool state);
    void setVelocityY(int velY);


private:
  int m_speed = 6;
  Vector2D velocity{0,0};

   bool isOnGround = false;
  void jump();
  std::unique_ptr<Audio> sound = std::make_unique<Audio>("../res/Audio/Sfx/jump.ogg");

protected:

};
