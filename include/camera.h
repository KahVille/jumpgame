#pragma once
#include <SDL2/SDL.h>
#include <iostream>
#include <memory>
#include <position.h>
#include "window.h"
class Camera
{
public:
    Camera(int x, int y, int w, int h);

    int positionX();
    int positionY();
    int width();
    int height();
    void move(int x, int y);
    void render();
private:

    std::unique_ptr<Position> position;
    int m_width{0};
    int m_height{0};
};
