#pragma once
#include <iostream>
#include <memory>

#include "sprite.h"
#include "collider.h"
#include "vector2d.h"
class GameObject
{
public:
    GameObject(bool active, bool visibility,Vector2D position,int width,int height);

    bool active() const;
    void setActive(bool state);

    bool visible() const;
    void SetVisible(bool state);
    int height() const;
    int width() const;
    void draw() const;
    int positionX() const;
    int positionY() const;

    void Move(Vector2D &pos);
    void setPosY(int posY);
    void setPosX(int posX);
    void PlayAnimation(int begin, int end,int row,int speed);
    void setSprite(const std::string &path);


    Vector2D getPosition();
private:
    bool isActive{false};
    bool isVisible{false};
    Vector2D m_position{0,0};
    int m_width{0},m_height{0};
    std::unique_ptr<Sprite> m_sprite {nullptr};


};
