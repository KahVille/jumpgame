#include "button.h"


kahviUI::Button::Button(int x, int y,int width,int height ,const std::string &buttontext) :
position (std::make_unique<Position>(x,y)),
m_width(width),
m_height(height),
text(std::make_unique<kahviUI::Text>(Window::renderer,"../res/Font/OpenSans-Regular.ttf", 28,buttontext,color)) {}

void kahviUI::Button::onClicked()
{
   // std::cout << "Button Clicked" << '\n';
}

void kahviUI::Button::display()
{
   if(isVisible()) {
   //sprite->Draw(*position,m_width,m_height);
   text->display(positionX(),positionY(),Window::renderer);
   }
}

void kahviUI::Button::setVisible(bool visibility)
{
    m_state = visibility;
}

bool kahviUI::Button::isVisible()
{
    return m_state;
}

void kahviUI::Button::setPosition(int x, int y)
{
    position->setxCoord(x);
    position->setyCoord(y);
}

int kahviUI::Button::positionX()
{
    return position->xCoord();
}

int kahviUI::Button::positionY()
{
    return position->yCoord();
}
