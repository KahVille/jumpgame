#include "vector2d.h"

Vector2D::Vector2D() {}

Vector2D::Vector2D(double x, double y) : x(x),y(y)
{

}

Vector2D Vector2D::operator+(const Vector2D &vec2) const
{
    return Vector2D(x+vec2.x,y + vec2.y);
}

Vector2D &Vector2D::operator+=(const Vector2D &vec2)
{
    return *this = *this + vec2;
}

Vector2D Vector2D::operator*(double value) const
{
    return Vector2D(x * value,y*value);
}

Vector2D &Vector2D::operator*=(double value)
{
    return *this = *this * value;
}
