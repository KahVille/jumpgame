#include "player.h"
#include "audio.h"
Player::Player(Vector2D position) : GameObject(true,true,position,110,180)
{
setSprite("../res/Graphics/Character/Player/playersprites.png");
}

void Player::draw() {
   GameObject::draw();
}

void Player::update() {

    velocity.y+=1;
   //  m_position +=velocity;
     Move(velocity);
     PlayAnimation(0,3,0,250);

}
void Player::OnGround(bool state)
{
    isOnGround=state;
}

void Player::setVelocityY(int velY)
{
    velocity.y = velY;
}

void Player::jump() {
    if(isOnGround ==true) {
    sound->PlaySound();
    velocity.y = -15;
}

}


void Player::pollEvent(SDL_Event &event) {

    switch (event.type) {
    case SDL_KEYDOWN:
        switch (event.key.keysym.sym) {
        case SDLK_s:

            break;
        case SDLK_w:
            //move camera up
            //playerCamera.move(0,mypos->yCoord()+ 50);
            break;
        case SDLK_SPACE:
            jump();
            break;
        case SDLK_a:

            velocity.x = -1 * m_speed;
            break;
        case SDLK_d:
           velocity.x =1 * m_speed;
            break;

        default:
            break;
        }
    break;

        //for smooth movement purposes reset the velocity
    case SDL_KEYUP:
        switch (event.key.keysym.sym) {
        case SDLK_s:
  if(velocity.y >0) velocity.y =0;
            break;
        case SDLK_w:
          //   playerCamera.move(mypos->xCoord(),mypos->yCoord()+ 50);
        case SDLK_a:
            if(velocity.x < 0) velocity.x =0;
            break;
        case SDLK_d:
           if(velocity.x > 0) velocity.x =0;
            break;
        case SDLK_SPACE:
            if(velocity.y < -5) velocity.y =-5;
        default:
            break;
        }

    default:
        break;
    }
}
