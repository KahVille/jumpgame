#include "text.h"
#include "window.h"

kahviUI::Text::Text(SDL_Renderer *renderer,const std::string &font_path, int font_size, const std::string &message_text, const SDL_Color &color)
{
    m_text = message_text;
    m_font_path = font_path;
    m_font_size = font_size;
    m_font_color = color;

    m_text_texture = loadFont(renderer,m_font_path,m_font_size,m_text,m_font_color);
    SDL_QueryTexture(m_text_texture,nullptr,nullptr,&m_text_rect.w,&m_text_rect.h);
}

kahviUI::Text::~Text()
{

}




void kahviUI::Text::display(int x,int y, SDL_Renderer *renderer) const
{
    m_text_rect.x = x;
    m_text_rect.y = y;
    SDL_RenderCopy(renderer,m_text_texture,nullptr,&m_text_rect);

}

SDL_Texture *kahviUI::Text::loadFont(SDL_Renderer *renderer, const std::string &font_path, int font_size, const std::string &message_text, const SDL_Color &color)
{


    TTF_Font *font = TTF_OpenFont(font_path.c_str(), font_size);
    if(!font) {
        std::cerr << "failed to load font\n";
    }
    auto text_surface = TTF_RenderText_Solid(font,message_text.c_str(),color);
    if(!text_surface) {
          std::cerr << "failed to create text_surface\n";
    }
    auto text_texture = SDL_CreateTextureFromSurface(renderer,text_surface);
    if(!text_texture) {
          std::cerr << "failed to create text_texture\n";
    }

    SDL_FreeSurface(text_surface);
    TTF_CloseFont(font);
    font = nullptr;
    return text_texture;
}

void kahviUI::Text::freeTexture() {
    //Free texture if it exists
    if( m_text_texture != nullptr )
    {
        SDL_DestroyTexture( m_text_texture );
        m_text_texture = nullptr;
    }
}

void kahviUI::Text::setText(const std::string &text, SDL_Renderer *renderer)
{
    if(m_text != text) {
        freeTexture();
        m_text_texture = loadFont(renderer,m_font_path,m_font_size,text,m_font_color);
    }
}
