#include "sprite.h"

Sprite::Sprite(const std::string &image_path=""){
  auto surface = IMG_Load(image_path.c_str());

      if(!surface) {
          std::cerr << "failed to create sprite surface\n";
      }
      sprite_texture = SDL_CreateTextureFromSurface(Window::renderer, surface);
      if(!sprite_texture) {
          std::cerr << "failed to create sprite texture\n";
      }
      SDL_QueryTexture(sprite_texture, NULL, NULL, &m_width, &m_height);
      sourceRect.x=0;
      sourceRect.y=0;
      sourceRect.w= m_width;
      sourceRect.h = m_height;
      currentFrame=0;
      SDL_FreeSurface(surface);
}



Sprite::~Sprite() {
    SDL_DestroyTexture(sprite_texture);
}

void Sprite::Draw(const Vector2D& position, int width, int height) const
{
    SDL_Rect destrect = {(int)position.x,(int)position.y,width,height};

  if(sprite_texture) {
         SDL_RenderCopy(Window::renderer,sprite_texture,&sourceRect,&destrect);
  }
  else {
    SDL_SetRenderDrawColor(Window::renderer,r,g,b,a);
    SDL_RenderFillRect(Window::renderer,&destrect);
  }
}

void Sprite::PlayAnimation(int beginframe,int endFrame, int row,int speed) {
    if(Uint32(animationDelay+speed) < SDL_GetTicks()) {
    if(currentFrame >= endFrame) currentFrame = beginframe;
    else
    currentFrame++;

    sourceRect.x = currentFrame *(m_width/4);
    sourceRect.y = row * (m_height/4);
    sourceRect.w = m_width / 4;
    sourceRect.h = m_height;
    animationDelay = SDL_GetTicks();
    }
}




