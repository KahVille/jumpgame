#include "mouse.h"
bool Mouse::click()
{
    return mouseClick;
}

int Mouse::positionX()
{
    return mouseX;
}

int Mouse::positionY()
{
    return mouseY;
}
void Mouse::HandleEvents(SDL_Event &event)
{
    switch (event.type) {
    case SDL_MOUSEMOTION:
    mouseX = event.motion.x;
    mouseY = event.motion.y;
    break;

    case SDL_MOUSEBUTTONDOWN:
      mouseClick=true;
      break;
    case SDL_MOUSEBUTTONUP:
    mouseClick=false;
    break;

    default:
    break;
    }
}
