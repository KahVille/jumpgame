#include "camera.h"

Camera::Camera(int x, int y, int w, int h) :
    position (std::make_unique<Position>(x,y)),
    m_width(w),
    m_height(h)
{
}

int Camera::positionX()
{
    return position->xCoord();
}

int Camera::positionY()
{
    return position->yCoord();
}

int Camera::width()
{
    return m_width;
}

int Camera::height()
{
    return m_height;
}

void Camera::move(int x, int y)
{
    position->setxCoord(x);
    position->setyCoord(y);
}

void Camera::render()
{
}
