#include "gameobject.h"

GameObject::GameObject(bool active, bool visibility, Vector2D position, int width, int height)
    : isActive(active),isVisible(visibility),m_position(position),m_width(width),m_height(height)
{}

bool GameObject::active() const {
    return isActive;
}
void GameObject::setActive(bool state) {
    isActive = state;
}

bool GameObject::visible() const {
    return isVisible;
}

void GameObject::SetVisible(bool state) {
    isVisible = state;
}

int GameObject::height() const {
    return m_height;
}
int GameObject::width() const {
    return m_width;
}

void GameObject::draw() const
{
    m_sprite->Draw(m_position,m_width,m_height);
}

int GameObject::positionX() const
{
    return m_position.x;
}

int GameObject::positionY() const
{
    return m_position.y;
}
Vector2D GameObject::getPosition() {
    return m_position;
}

void GameObject::Move(Vector2D &pos)
{
    m_position += pos;
}

void GameObject::setPosY(int posY)
{
    m_position.y = posY;
}

void GameObject::setPosX(int posX)
{
    m_position.x = posX;
}

void GameObject::PlayAnimation(int begin, int end, int row, int speed)
{
    m_sprite->PlayAnimation(begin,end,row,speed);
}

void GameObject::setSprite(const std::string &path)
{
    m_sprite = std::make_unique<Sprite>(path);
}
