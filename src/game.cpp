#include "game.h"

void Game::update() {
if(player!=nullptr)player->update();

if(player->getPosition().y + player->height() > ground->getPosition().y) {
    player->setPosY(ground->getPosition().y - player->height());
    player->setVelocityY(0);
    player->OnGround(true);
}
else {
    player->OnGround(false);
}


}

void Game::render() {

if(player!=nullptr)player->draw();
ground->draw();
}

Game::Game() :
               gameWindow(std::make_unique<Window>("JumpGame",1366,768)),
               m_closed(false),
               player(std::make_unique<Player>(Vector2D{300,300})),
               ground(std::make_unique<GameObject>(true, true, Vector2D{0,700}, 1366, 20))
                {
                   ground->setSprite("../res/Graphics/Platform/platformdebug.png");
                }

void Game::pollEvent(SDL_Event &event) {
     gameWindow->pollEvents(event);
    if(player!=nullptr) player->pollEvent(event);
    //mouse->HandleEvents(event);
}

void Game::run() {
    while (!(gameWindow->isClosed() || this->isClosed()) ) {
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            pollEvent(event);
        }
      this->update();
      this->render();
      gameWindow->clear();
      SDL_Delay(16);
    }

}
