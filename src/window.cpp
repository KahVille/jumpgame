#include "window.h"


SDL_Renderer *Window::renderer = nullptr;

Window::Window(const std::string &title, int width, int height) :
   title(title), width(width), height(height)
{
    this->closed = !init();
}
Window::~Window()
{
    SDL_DestroyRenderer(this->renderer);
    SDL_DestroyWindow(this->window);
    TTF_Quit();
    IMG_Quit();
    Mix_Quit();
    SDL_Quit();

}

bool Window::isClosed() const
{
    return this->closed;
}

bool Window::init()
{
    if(SDL_Init(SDL_INIT_VIDEO || SDL_INIT_AUDIO) !=0) {
        std::cerr << "failed to init sdl window\n";
        return false;
    }

    if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
        std::cerr << "failed to init sdl image\n";
        return false;
    }
    if(TTF_Init() == -1) {
        std::cerr << "failed to init sdl ttf\n";
        return false;
    }

    // load support for the OGG
    if(Mix_Init(MIX_INIT_OGG) != MIX_INIT_OGG) {
    std::cerr << "failed to init sdl mixer\n";
    return false;
}


    this->window = SDL_CreateWindow(this->title.c_str(),SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,this->width,this->height,SDL_WINDOW_FULLSCREEN_DESKTOP);
    if( window == nullptr )
    {
      std::cerr << "Window could not be created!\n";
      return false;
    }

    this->renderer = SDL_CreateRenderer(this->window,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);//SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
    if(this->renderer == nullptr)
    {
      std::cerr << "Renderer could not be created!\n";
      return false;
    }

    return true;

}

void Window::pollEvents(SDL_Event &event)
{
        switch (event.type) {
        case SDL_QUIT:
            this->closed = true;
            break;

        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
                this->closed = true;
                break;

            default:
                break;
            }

        break;

        default:
            break;
        }
}

void Window::clear() const
{
    SDL_RenderPresent(this->renderer);
    SDL_SetRenderDrawColor(this->renderer,255,255,255,255);
    SDL_RenderClear(this->renderer);
}

void Window::setResolution(int width, int height)
{
    SDL_RenderSetLogicalSize(this->renderer,width,height);
}
