#include "position.h"

Position::Position(int x,int y): m_x(x),m_y(y) {}

int Position::xCoord() const {return m_x;}
int Position::yCoord() const {return m_y;}

void Position::setxCoord(double x) {m_x =x;}
void Position::setyCoord(double y) {m_y = y;}

int Position::xVelocity() const {return m_xvelocity;}
int Position::yVelocity() const {return m_yvelocity;}

void Position::setxVelocity(double xvel) {m_xvelocity = xvel;}
void Position::setyVelocity(double yvel) {m_yvelocity = yvel;}
void Position::update() {
m_yvelocity-=1;
m_x -=m_xvelocity;
m_y -=m_yvelocity;

}
