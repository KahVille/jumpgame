#include "audio.h"
#include <iostream>
Audio::Audio(const std::string &filename) {
    // open 44.1KHz, signed 16bit, system byte order,
    //      stereo audio, using 1024 byte chunks
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)==-1) {
        std::cout << "cant open audio\n";
    }

    sound=Mix_LoadWAV(filename.c_str());
    if(!sound) {
        printf("Mix_LoadWAV: %s\n", Mix_GetError());
        // handle error
    }

    // allocate 16 mixing channels
    Mix_AllocateChannels(16);

    // set channel 1 to half volume
    Mix_Volume(1,MIX_MAX_VOLUME/2);

    // print the average volume
    printf("Average volume is %d\n",Mix_Volume(-1,-1));
}

Audio::~Audio()
{
    Mix_CloseAudio();
}

void Audio::PlaySound() {
    if(Mix_PlayChannel(-1, sound, 0)==-1) {
        printf("Mix_PlayChannel: %s\n",Mix_GetError());
        // may be critical error, or maybe just no channels were free.
        // you could allocated another channel in that case...
    }
}
